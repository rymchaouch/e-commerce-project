import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { JoinComponent } from '../join/join.component';
import { PayerComponent } from '../payer/payer.component';
import { AppModule } from '../app.module';

@Component({
  selector: 'app-account',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class AccountComponent implements OnInit {

  constructor(public dialog: MatDialog , private dialogRef: MatDialogRef<AppModule>,) { }


  ngOnInit() {
  }
 openDialog(): void {
    const dialogRef = this.dialog.open(PayerComponent, {
    });
    

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
    });
    
  }
  onNoClick(): void {
    this.dialogRef.close();
   
 }
}
